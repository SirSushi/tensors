#ifndef TENSOR_H
#define TENSOR_H

#include "Dimension.h"

class Tensor {
    Dimension m_Dim;
    double* m_Data;
    bool m_ManageData;

private:
    Tensor(Dimension dim);

public:
    Tensor(const Tensor& Tensor);
    Tensor(Dimension dim, double* data);
    ~Tensor();

    Tensor operator+(const Tensor& other) const;
};

#endif
