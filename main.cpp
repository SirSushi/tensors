#include "Operations/Constant.h"
#include "Operations/Operators.h"

int main() {
	Constant c1(Dimension(1, {3}), {1, 2, 3});
	Constant c2(Dimension(1, {3}), {4, 5, 6});

	Add add = c1 + c2;
	Tensor result = add.resolve();
	return 0;
}