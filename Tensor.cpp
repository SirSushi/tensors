#include "Tensor.h"

Tensor::Tensor(Dimension dim)
:m_Dim(dim)
,m_Data(new double[dim.length()])
,m_ManageData(true)
{}

Tensor::Tensor(Dimension dim, double* data)
:m_Dim(dim)
,m_Data(data)
,m_ManageData(false)
{}

Tensor::~Tensor() {
	if (m_ManageData) delete[] m_Data;
}

Tensor Tensor::operator+(const Tensor& other) const {
	Tensor result(m_Dim);
	int length = m_Dim.length();
	for (int i = 0; i < length; ++i) {
		result.m_Data[i] = m_Data[i] + other.m_Data[i];
	}
}
