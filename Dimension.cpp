Dimension::Dimension(int count, int* list)
:m_Count(count)
,m_List(list) {

}

int Dimension::length() {
	length = 0;
	for (int i = 0; i < m_Count; ++i) {
		length += m_List[i];
	}
	return length;
}

bool Dimension::operator==(const Dimension& other) {
	if (m_Count != other.m_Count) return false;
	for (int i = 0; i < m_Count; ++i) {
		if (m_List[i] != other.m_List[i]) return false;
	}
	return true;
}
bool Dimension::operator!=(const Dimension& other) {
	return !(*this == other);
}

DimensionOrderException::DimensionOrderException(const Dimension& dim1, const Dimension& dim2) {}