#ifndef DIMENSION_H
#define DIMENSION_H

class Dimension {
    int m_Count;
    int* m_List;

public:
    Dimension(int count, int* list);
    int length();

    bool operator==(const Dimension& other);
    bool operator!=(const Dimension& other);
};

class DimensionOrderException {
    Dimension m_Dim1;
    Dimension m_Dim2;
public:
    DimensionOrderException(const Dimension& dim1, const Dimension& dim2);
};

#endif