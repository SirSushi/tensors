#ifndef VALUE_H
#define VALUE_H

#include "Operation.h"
#include "../Tensor.h"

class Value : public Operation {
protected:
	Tensor m_Tensor;

public:
	Value(Dimension dim, double* values);
};

#endif