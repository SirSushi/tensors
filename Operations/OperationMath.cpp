#include "OperationMath.h"

MathOperation::MathOperation(const Operation& a, const Operation& b)
:m_OperandA(a)
,m_OperandB(b) {
	if (a.getDim() != b.getDim())
		throw DimensionOrderException(a.getDim(), b.getDim());
	m_Dim = a.getDim();
}

Dimension MathOperation::getDim() const {
	return m_Dim;
}