#ifndef OPERATION_MATH_H
#define OPERATION_MATH_H

#include "Operation.h"

class MathOperation : public Operation {
protected:
	Dimension m_Dim;
	const Operation& m_OperandA;
	const Operation& m_OperandB;
public:
	MathOperation(const Operation& a, const Operation& b);
	virtual Dimension getDim() const override;
};

#endif