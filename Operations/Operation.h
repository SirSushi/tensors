#ifndef OPERATION_H
#define OPERATION_H

#include "../Dimension.h"
#include "../Tensor.h"

class Operation {
public:
    virtual Dimension getDim() const = 0;
    virtual const Tensor& resolve() const = 0;
};

#endif