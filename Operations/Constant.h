#ifndef CONSTANT_H
#define CONSTANT_H

#include "Value.h"

class Constant : public Value {
public:
	virtual const Tensor& resolve() const override;
};

#endif