#ifndef OPERATION_ADD_H
#define OPERATION_ADD_H

#include "OperationMath.h"

class Add : public MathOperation {
public:
	using MathOperation::MathOperation;
	virtual const Tensor& resolve() const override;
};

#endif