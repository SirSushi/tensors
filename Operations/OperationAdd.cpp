#include "OperationAdd.h"

const Tensor& Add::resolve() const {
	return m_OperandA.resolve() + m_OperandB.resolve();
}